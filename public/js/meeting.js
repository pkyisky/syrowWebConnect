'use strict';

var Meeting = function(socketioHost) {
  var exports = {};

  var _isInitiator = false;
  var _localStream;
  var _remoteStream;
  var _turnReady;

  // var _pcConfig = {'iceServers': [
  //   {url: 'turn:numb.viagenie.ca',credential: 'muazkh',username: 'webrtc@live.com'},
  //   {'url': 'stun:stun.l.google.com:19302'},
  //   {url: 'turn:turn.syrow.com:80',credential: '',username: ''}
  //   ]
  // };


  var _pcConfig = {'iceServers': [
    { url: 'turn:numb.viagenie.ca',credential: 'muazkh',username: 'webrtc@live.com'},
    {'url': 'stun:stun.l.google.com:19302'}
    ]
  };


  var _constraints = {
    video: true,
    audio: true
  };
  var _defaultChannel;
  var _privateAnswerChannel;
  var _offerChannels = {};
  var _opc = {};
  var _apc = {};
  var _sendChannel = {};
  var _room;
  var _myID;
  var _onRemoteVideoCallback;
  var _onLocalVideoCallback;
  var _onChatMessageCallback;
  var _onChatReadyCallback;
  var _onChatNotReadyCallback;
  var _onParticipantHangupCallback;
  var _host = socketioHost;
  var currentConnection;

  function joinRoom(name, audio_video) {
    _room = name;

    _myID = generateID();
    console.log('Generated ID: ' + _myID);

    // Open up a default communication channel
    initDefaultChannel();

    if (_room !== '') {
      console.log('Create or join room', _room);
      _defaultChannel.emit('create or join', {
        room: _room,
        from: _myID
      });
    }

    // Open up a private communication channel
    initPrivateChannel();
    if (audio_video == 'audio') {
      _constraints = {
        audio: true,
        video: false
      };
    } else {
      _constraints = {
        video: true,
        audio: true
      };
    }

    if (navigator.mediaDevices==undefined) {
      navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia;
    }else {
      navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mediaDevices.getUserMedia
    }

    // Get local media data
    navigator.getUserMedia(_constraints, handleUserMedia, handleUserMediaError);

    window.onbeforeunload = function(e) {
      _defaultChannel.emit('message', {
        type: 'bye',
        from: _myID
      });
    }
  }

  function changeConstraints(constraints) {
    _constraints = constraints
    navigator.getUserMedia = navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mediaDevices.getUserMedia ||
      navigator.mediaDevices.getUserMedia;
    // Get local media data
    navigator.getUserMedia(_constraints, handleUserMedia, handleUserMediaError);
  }


  function sendChatMessage(message) {
    console.log("Sending " + message)
    for (var channel in _sendChannel) {
      if (_sendChannel.hasOwnProperty(channel)) {
        _sendChannel[channel].send(message);
      }
    }
  }

  function toggleMic() {
    var tracks = _localStream.getTracks();
    for (var i = 0; i < tracks.length; i++) {
      if (tracks[i].kind == "audio") {
        tracks[i].enabled = !tracks[i].enabled;
      }
    }
  }

  function toggleVideo() {
    var tracks = _localStream.getTracks();
    for (var i = 0; i < tracks.length; i++) {
      if (tracks[i].kind == "video") {
        tracks[i].enabled = !tracks[i].enabled;
      }
    }
  }


  function onRemoteVideo(callback) {
    _onRemoteVideoCallback = callback;
  }

  function onLocalVideo(callback) {
    _onLocalVideoCallback = callback;
  }

  function onChatReady(callback) {
    _onChatReadyCallback = callback;
  }

  function onChatNotReady(callback) {
    _onChatNotReadyCallback = callback;
  }

  function onChatMessage(callback) {
    _onChatMessageCallback = callback;
  }

  function onParticipantHangup(callback) {
    _onParticipantHangupCallback = callback;
  }

  function initDefaultChannel() {
    _defaultChannel = openSignalingChannel(''); // creating a namespace for socket.io
    _defaultChannel.on('created', function(room) {
      console.log('Created room ' + room);
      _isInitiator = true;
    });

    _defaultChannel.on('join', function(room) {
      console.log('Another peer made a request to join room ' + room);
    });

    _defaultChannel.on('joined', function(room) {
      console.log('This peer has joined room ' + room);
    });

    _defaultChannel.on('message', function(message) {
      console.log('Client received message:', message);
      if (message.type === 'newparticipant') {
        var partID = message.from;

        // Open a new communication channel to the new participant
        _offerChannels[partID] = openSignalingChannel(partID);
        console.log(_offerChannels);
        // Wait for answers (to offers) from the new participant
        _offerChannels[partID].on('message', function(msg) {
          if (msg.dest === _myID) {
            if (msg.type === 'answer') {
              _opc[msg.from].setRemoteDescription(new RTCSessionDescription(msg.snDescription),
                setRemoteDescriptionSuccess,
                setRemoteDescriptionError);
            } else if (msg.type === 'candidate') {
              var candidate = new RTCIceCandidate({
                sdpMLineIndex: msg.label,
                candidate: msg.candidate
              });
              console.log('got ice candidate from ' + msg.from);
              _opc[msg.from].addIceCandidate(candidate, addIceCandidateSuccess, addIceCandidateError);
            }
          }
        });

        // Send an offer to the new participant
        createOffer(partID);

      } else if (message.type === 'bye') {
        hangup(message.from);
      }
    });
  }

  function initPrivateChannel() {
    // Open a private channel (namespace = _myID) to receive offers
    _privateAnswerChannel = openSignalingChannel(_myID);
    console.log(_privateAnswerChannel);
    // Wait for offers or ice candidates
    console.log('in initPrivateChannel');
    _privateAnswerChannel.on('message', function(message) {
      if (message.dest === _myID) {
        if (message.type === 'offer') {
          var to = message.from;
          console.log("entering to create answer");
          createAnswer(message, _privateAnswerChannel, to);
        } else if (message.type === 'candidate') {
          var candidate = new RTCIceCandidate({
            sdpMLineIndex: message.label,
            candidate: message.candidate
          });
          _apc[message.from].addIceCandidate(candidate, addIceCandidateSuccess, addIceCandidateError);
        }
      }
    });
  }

  function requestTurn(turn_url) {
    alert('requestTurn', turn_url)
    var turnExists = false;
    for (var i in _pcConfig.iceServers) {
      if (_pcConfig.iceServers[i].url.substr(0, 5) === 'turn:') {
        turnExists = true;
        _turnReady = true;
        break;
      }
    }

    if (!turnExists) {
      console.log('Getting TURN server from ', turn_url);
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
          var turnServer = JSON.parse(xhr.responseText);
          console.log('Got TURN server: ', turnServer);
          _pcConfig.iceServers.push({
            'url': 'turn:' + turnServer.username + '@' + turnServer.turn,
            'credential': turnServer.password
          });
          _turnReady = true;
        }
      }
      xhr.open('GET', turn_url, true);
      xhr.send();
    }
  }

  function addRemoteVideo(stream, from) {
    // call the callback
    _onRemoteVideoCallback(stream, from);
  }

  function generateID() {
    var s4 = function() {
      return Math.floor(Math.random() * 0x10000).toString(16);
    };
    return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
  }

  function openSignalingChannel(channel) {
    var namespace = _host + '/' + channel;
    var sckt = io.connect(namespace);
    return sckt;
  }


  function createOffer(participantId) {
    console.log('Creating offer for peer ' + participantId);
    _opc[participantId] = new RTCPeerConnection(_pcConfig);

    // console.log(currentConnection);
    _opc[participantId].onicecandidate = handleIceCandidateAnswerWrapper(_offerChannels[participantId], participantId);
    _opc[participantId].onaddstream = handleRemoteStreamAdded(participantId);
    _opc[participantId].onremovestream = handleRemoteStreamRemoved;
    console.log('local stream is getting added to create offer');
    _opc[participantId].addStream(_localStream);
    // currentConnection =  _opc[participantId];

    try {
      // Reliable Data Channels not yet supported in Chrome
      _sendChannel[participantId] = _opc[participantId].createDataChannel("sendDataChannel", {
        reliable: false
      });
      _sendChannel[participantId].onmessage = handleMessage;
      console.log('Created send data channel');
    } catch (e) {
      alert('Failed to create data channel. ' + 'You need Chrome M25 or later with RtpDataChannel enabled');
      console.log('createDataChannel() failed with exception: ' + e.message);
    }
    _sendChannel[participantId].onopen = handleSendChannelStateChange(participantId);
    _sendChannel[participantId].onclose = handleSendChannelStateChange(participantId);

    var onSuccess = function(participantId) {
      return function(sessionDescription) {
        var channel = _offerChannels[participantId];

        // Set Opus as the preferred codec in SDP if Opus is present.
        sessionDescription.sdp = preferOpus(sessionDescription.sdp);
        _opc[participantId].setLocalDescription(sessionDescription, setLocalDescriptionSuccess, setLocalDescriptionError);
        console.log('Sending offer to channel ' + channel.name);
        channel.emit('message', {
          snDescription: sessionDescription,
          from: _myID,
          type: 'offer',
          dest: participantId
        });
      }
    }

    _opc[participantId].createOffer(onSuccess(participantId), handleCreateOfferError);
  }

  function createAnswer(sdp, cnl, to) {
    console.log('Creating answer for peer ' + to);
    _apc[to] = new RTCPeerConnection(_pcConfig);
    _apc[to].onicecandidate = handleIceCandidateAnswerWrapper(cnl, to);
    _apc[to].onaddstream = handleRemoteStreamAdded(to);
    _apc[to].onremovestream = handleRemoteStreamRemoved;
    console.log('local stream is getting added to create Answer');
    _apc[to].addStream(_localStream);
    _apc[to].setRemoteDescription(new RTCSessionDescription(sdp.snDescription), setRemoteDescriptionSuccess, setRemoteDescriptionError);

    _apc[to].ondatachannel = gotReceiveChannel(to);

    var onSuccess = function(channel) {
      return function(sessionDescription) {
        87
        // Set Opus as the preferred codec in SDP if Opus is present.
        sessionDescription.sdp = preferOpus(sessionDescription.sdp);

        _apc[to].setLocalDescription(sessionDescription, setLocalDescriptionSuccess, setLocalDescriptionError);
        console.log('Sending answer to channel ' + channel.name);
        channel.emit('message', {
          snDescription: sessionDescription,
          from: _myID,
          type: 'answer',
          dest: to
        });
      }
    }

    _apc[to].createAnswer(onSuccess(cnl), handleCreateAnswerError);
  }

  function hangup(from) {
    console.log('Bye received from ' + from);

    if (_opc.hasOwnProperty(from)) {
      _opc[from].close();
      _opc[from] = null;
    }

    if (_apc.hasOwnProperty(from)) {
      _apc[from].close();
      _apc[from] = null;
    }

    _onParticipantHangupCallback(from);
  }

  function handleUserMedia(stream) {
    console.log('Adding local stream');
    console.log(stream);
    _onLocalVideoCallback(stream);

    _localStream = stream;
    console.log(_localStream.getTracks());
    _defaultChannel.emit('message', {
      type: 'newparticipant',
      from: _myID
    });
  }

  function handleRemoteStreamRemoved(event) {
    console.log('Remote stream removed. Event: ', event);

  }

  function handleRemoteStreamAdded(from) {
    return function(event) {
      console.log('Remote stream added');

      addRemoteVideo(event.stream, from);
      _remoteStream = event.stream;
    }
  }

  function handleIceCandidateAnswerWrapper(channel, to) {
    return function handleIceCandidate(event) {
      console.log('handleIceCandidate event');
      if (event.candidate) {
        channel.emit('message', {
          type: 'candidate',
          label: event.candidate.sdpMLineIndex,
          id: event.candidate.sdpMid,
          candidate: event.candidate.candidate,
          from: _myID,
          dest: to
        });

      } else {
        console.log('End of candidates.');
      }
    }
  }

  function setLocalDescriptionSuccess() {}

  function setRemoteDescriptionSuccess() {}

  function addIceCandidateSuccess() {}

  function gotReceiveChannel(id) {
    return function(event) {
      console.log('Receive Channel Callback');
      _sendChannel[id] = event.channel;
      _sendChannel[id].onmessage = handleMessage;
      _sendChannel[id].onopen = handleReceiveChannelStateChange(id);
      _sendChannel[id].onclose = handleReceiveChannelStateChange(id);
    }
  }

  function handleMessage(event) {
    console.log('Received message: ' + event.data);
    _onChatMessageCallback(event.data);
  }

  function handleSendChannelStateChange(participantId) {
    return function() {
      var readyState = _sendChannel[participantId].readyState;
      console.log('Send channel state is: ' + readyState);

      // check if we have at least one open channel before we set hat ready to false.
      var open = checkIfOpenChannel();
      enableMessageInterface(open);
    }
  }

  function handleReceiveChannelStateChange(participantId) {
    return function() {
      var readyState = _sendChannel[participantId].readyState;
      console.log('Receive channel state is: ' + readyState);

      // check if we have at least one open channel before we set hat ready to false.
      var open = checkIfOpenChannel();
      enableMessageInterface(open);
    }
  }

  function checkIfOpenChannel() {
    var open = false;
    for (var channel in _sendChannel) {
      if (_sendChannel.hasOwnProperty(channel)) {
        open = (_sendChannel[channel].readyState == "open");
        if (open == true) {
          break;
        }
      }
    }

    return open;
  }

  function enableMessageInterface(shouldEnable) {
    if (shouldEnable) {
      _onChatReadyCallback();
    } else {
      _onChatNotReadyCallback();
    }
  }

  // ERROR HANDLERS

  function handleCreateOfferError(event) {
    console.log('createOffer() error: ', event);
  }

  function handleCreateAnswerError(event) {
    console.log('createAnswer() error: ', event);
  }

  function handleUserMediaError(error) {
    console.log('getUserMedia error: ', error);
    alert('Please use only one camera at a time')
  }

  function setLocalDescriptionError(error) {
    console.log('setLocalDescription error: ', error);
  }

  function setRemoteDescriptionError(error) {
    console.log('setRemoteDescription error: ', error);
  }

  function addIceCandidateError(error) {}

  function preferOpus(sdp) {
    var sdpLines = sdp.split('\r\n');
    var mLineIndex;
    // Search for m line.
    for (var i = 0; i < sdpLines.length; i++) {
      if (sdpLines[i].search('m=audio') !== -1) {
        mLineIndex = i;
        break;
      }
    }
    if (mLineIndex === null || mLineIndex === undefined) {
      return sdp;
    }

    // If Opus is available, set it as the default in m line.
    for (i = 0; i < sdpLines.length; i++) {
      if (sdpLines[i].search('opus/48000') !== -1) {
        var opusPayload = extractSdp(sdpLines[i], /:(\d+) opus\/48000/i);
        if (opusPayload) {
          sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], opusPayload);
        }
        break;
      }
    }

    // Remove CN in m line and sdp.
    sdpLines = removeCN(sdpLines, mLineIndex);

    sdp = sdpLines.join('\r\n');
    return sdp;
  }

  function extractSdp(sdpLine, pattern) {
    var result = sdpLine.match(pattern);
    return result && result.length === 2 ? result[1] : null;
  }

  // Set the selected codec to the first in m line.
  function setDefaultCodec(mLine, payload) {
    var elements = mLine.split(' ');
    var newLine = [];
    var index = 0;
    for (var i = 0; i < elements.length; i++) {
      if (index === 3) { // Format of media starts from the fourth.
        newLine[index++] = payload; // Put target payload to the first.
      }
      if (elements[i] !== payload) {
        newLine[index++] = elements[i];
      }
    }
    return newLine.join(' ');
  }

  // Strip CN from sdp before CN constraints is ready.
  function removeCN(sdpLines, mLineIndex) {
    var mLineElements = sdpLines[mLineIndex].split(' ');
    // Scan from end for the convenience of removing an item.
    for (var i = sdpLines.length - 1; i >= 0; i--) {
      var payload = extractSdp(sdpLines[i], /a=rtpmap:(\d+) CN\/\d+/i);
      if (payload) {
        var cnPos = mLineElements.indexOf(payload);
        if (cnPos !== -1) {
          // Remove CN payload from m line.
          mLineElements.splice(cnPos, 1);
        }
        // Remove CN line in sdp
        sdpLines.splice(i, 1);
      }
    }

    sdpLines[mLineIndex] = mLineElements.join(' ');
    return sdpLines;
  }

  exports.joinRoom = joinRoom;
  exports.toggleMic = toggleMic;
  exports.toggleVideo = toggleVideo;
  exports.onLocalVideo = onLocalVideo;
  exports.onRemoteVideo = onRemoteVideo;
  exports.onChatReady = onChatReady;
  exports.onChatNotReady = onChatNotReady;
  exports.onChatMessage = onChatMessage;
  exports.sendChatMessage = sendChatMessage;
  exports.onParticipantHangup = onParticipantHangup;
  exports.handleUserMedia = handleUserMedia;
  exports.currentConnection = _opc;
  exports.changeConstraints = changeConstraints
  // exports._opc[participantId]=
  // exports._localStream=_localStream;
  return exports;

};
