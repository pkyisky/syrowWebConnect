'use strict';

var room = window.location.pathname.match(/([^\/]*)\/*$/)[1];
var localVideoElement = null;
var remoteVideoElement = null;
var audioElement = document.querySelector('audio');
var recorder, recorder1, recorder2;
var localStream, remoteStream;
var mediaStream = null;
var url_string = window.location.href
var url = new URL(window.location.href);
var audio_video = url.searchParams.get("audio_video");
var windowColor = url.searchParams.get("windowColor");
var iconColor = url.searchParams.get("iconColor");
var isAgent = url.searchParams.get("agent");
var isUserMobile = url.searchParams.get("userMob");
var canvas_agent, ctx_agent, w = 800,
  h = 500;
var myParticipantId;
var meeting;
var host = HOST_ADDRESS;
var screen = false;
var connection, mySender;
console.log(audio_video);

var videMicOn = true;
var audiMicOn = true;

var server_address = 'https://app.syrow.com';
// var server_address = 'http://192.168.0.110:8080';

function postJsError(msg, url, line, stack) {
  var error_text = msg || '';
  var url = url || '';
  var line = line || '';
  var stack = stack || '';

  if (stack.length > 500) {
    stack = stack.slice(0, 500)
  }

  if (error_text.length > 200) {
    error_text = error_text.slice(0, 200)
  }

  var obj_to_send = {
    url: url,
    line: line,
    stack: stack,
    error_text: error_text
  }

  if (UID) {
    obj_to_send.uid = UID
  }

  // var apiErrorUrl = 'http://localhost:8080/api/support/error/'
  var apiErrorUrl = server_address+'/api/support/error/';

  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      console.log('error posted');
    }
  };
  request.open('POST', apiErrorUrl, true);
  request.setRequestHeader("Content-type", "application/json");
  request.send(JSON.stringify(obj_to_send));
}

window.addEventListener('error', function(e) {
  var errorText = [
    e.message,
    'URL: ' + e.filename,
    'Line: ' + e.lineno + ', Column: ' + e.colno,
    'Stack: ' + (e.error && e.error.stack || '(no stack trace)')
  ].join('\n');

  var msg = e.message
  var url = e.filename
  var line = 'Line:' + e.lineno + ', Column: ' + e.colno
  var stack = e.error && e.error.stack || '(no stack trace)'
  postJsError(msg, url, line, stack);
})


function stopStreamRecording(recorder, streamType, call_back) {
  console.log('recorder', recorder);
  if (recorder != undefined) {
    console.log('state', recorder.getState());
    if (recorder.getState() == 'recording') {
      recorder.stopRecording(function() {
        var blob2 = recorder.getBlob();
        console.log(blob2);
        postFileForSave(blob2, streamType, call_back);
        console.log('posting ' + streamType)
      });
    } else {
      console.log('recorder state is closed');
    }
  }
}


if (isAgent == 'true') {
  isAgent = true
} else {
  isAgent = false
}

if (isUserMobile == 'true') {
  isUserMobile = true
} else {
  isUserMobile = false
}

function closeChat() {
  setTimeout(function() {
    if (isAgent) {
      stopStreamRecording(recorder1, 'local');
      stopStreamRecording(recorder, 'agent');
      stopStreamRecording(recorder2, 'screen');
    }
    // remoteStream.getTracks().forEach(track => track.stop());
    localStream.getTracks().forEach(track => track.stop());
  }, 2000);
}

function closeChatAgent(call_back) {
  console.log('agent left recieved');
    setTimeout(function() {
      // console.log(recorder1);
      // console.log(recorder);
      // console.log(recorder2);
      setTimeout(function() {
        if (isAgent) {
          stopStreamRecording(recorder, 'local', call_back);
        }
        setTimeout(function() {
          if (isAgent) {
            stopStreamRecording(recorder2, 'screen', call_back);
          }
          setTimeout(function() {
            if (isAgent) {
              stopStreamRecording(recorder1, 'agent', call_back);
            }
            localStream.getTracks().forEach(track => track.stop());
          }, 1000);
        }, 1000);
      }, 1000);

    }, 1000);
}


var scope = angular.element(document.getElementById('main')).scope();
// console.log(scope);


var app = angular.module('app', ['rzModule']);
app.controller('main', function($scope) {
  $scope.mic = true;
  $scope.pause = true;
  $scope.screen = false;
  $scope.isMicOn = true;
  $scope.isAudioMute = false;
  $scope.audio_video = audio_video;
  $scope.windowColor = windowColor
  $scope.isAgent = isAgent
  $scope.isUserMobile = isUserMobile
  $scope.isFullFrame = true;
  $scope.isScreenOn = false;
  $scope.isVideoOn = true;
  $scope.rotateIcons = {
    value: false
  }

  if (!$scope.isAgent) {
    if ($scope.isUserMobile) {
      $scope.videoClass = "videoMobile"
    } else {
      $scope.videoClass = "videoFull"
    }
  }


  //check if mobile
  $scope.conditions = {
    hideShareScreenBtn:false,
  }

  if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
     // Do Firefox-related activities
   }else if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {

   }else {
     $scope.conditions.hideShareScreenBtn = true;
   }

   if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $scope.conditions.hideShareScreenBtn = true;
   }

  window.addEventListener("message", receiveMessage, false);

  function receiveMessage(event) {
    console.log(event);
    if (event.data == 'userleft') {
      console.log('visitor left');
      if (videMicOn) {
        meeting.toggleMic()
      }
      if (audiMicOn) {
        meeting.toggleMic()
      }
      if (recorder1 != undefined) {
        // closeChat();
      }
      event.source.postMessage("replyToUseruserleft", event.origin);
    } else if (event.data == 'refreshIframeVistorSide') {
      console.log('refresh Iframe Vistor Side');
      location.reload()
    } else if (event.data == 'rotateIcons') {
      // alert($scope.isAgent)
      if (!$scope.isAgent) {
        $scope.rotateIcons.value = !$scope.rotateIcons.value
        if ($scope.rotateIcons.value) {
          for (var i = 0; i < document.getElementsByClassName('toggle').length; i++) {
            document.getElementsByClassName('toggle')[i].classList.add("rotateIt");
          }

        } else {
          for (var i = 0; i < document.getElementsByClassName('toggle').length; i++) {
            document.getElementsByClassName('toggle')[i].classList.remove("rotateIt");
          }

        }
      }
    } else if (event.data == 'refreshIframeVistorSide') {
      console.log('refresh Iframe Vistor Side');
      location.reload()
    } else if (event.data == 'captureImage') {
      console.log('capture Image');
      ctx_agent.fillRect(0, 0, w, h);
      ctx_agent.drawImage(remoteVideo, 0, 0, w, h);
      event.source.postMessage(UID + "*" + canvas_agent.toDataURL(),
        event.origin);
    } else if (event.data == 'agentLeft') {
      console.log('agentLeft');
      closeChatAgent(function(typOfStream) {
        if (typOfStream == 'agent') {
          event.source.postMessage('agent_file_saved', event.origin);
        } else if (typOfStream == 'local') {
          event.source.postMessage('visitor_file_saved', event.origin);
        }
      });

      var localVideo = document.getElementById("localVideo");
      if (localVideo) {
        localVideo.muted = true;
      }
      var remoteVideo = document.getElementById("remoteVideo");
      if (remoteVideo) {
        remoteVideo.muted = true;
      }

      var localAudio = document.getElementById("localAudio");
      if (localAudio) {
        localAudio.muted = true;
      }
    }
  }

  $scope.isMicOn = true;
  $scope.handleMicOn = function() {
    audiMicOn = true;
    $scope.isMicOn = true;
    meeting.toggleMic()
  }
  $scope.handleMicOff = function() {
    audiMicOn = false;
    $scope.isMicOn = false;
    meeting.toggleMic()
  }
  $scope.hideVisitorVideo = function() {
    // if(!$scope.alreadyDone){
    $scope.isVideoOn = !$scope.isVideoOn
    if ($scope.isVideoOn) {

      if (!$scope.isAgent) {
        parent.postMessage("calledToShowVideo", '*');
        // $scope.rotateIcons.value=false;
      } else {
        parent.postMessage("calledToShowVisitorVideo", '*');

      }
      document.querySelector('#localVideoWrap').style.display = 'block'
      // document.getElementById('mainIconsFrame').style.padding='3vh'
    } else {

      if (!$scope.isAgent) {
        parent.postMessage("calledToHideVideo", '*');
      } else {
        parent.postMessage("calledToHideVisitorVideo", '*');
      }
      document.querySelector('#localVideoWrap').style.display = 'none'
      // document.getElementById('mainIconsFrame').style.padding='10vh'
    }
    setTimeout(function() {
      meeting.toggleVideo();
    }, 1000);
    // }
  }
})

function postFileForSave(blob, agent, call_back) {

  // alert('posting fileeeeeeeeee')

  console.log(UID);
  if (audio_video == 'audio') {
    var fileName = agent + UID + '.mp3';
    var file = new File([blob], fileName, {
      type: 'audio/mp3'
    });
  } else {
    var fileName = agent + UID + '.webm';
    var file = new File([blob], fileName, {
      type: 'video/webm'
    });
  }
  console.log(fileName);

  if (audio_video == "audio") {
    var element = audioElement;
  } else {
    var element = localVideoElement;
  }
  element.srcObject = mediaStream;
  element.poster = '/ajax-loader.gif';
  var fileName2 = fileName;
  xhr('https://fsr.syrow.com/fileup/', blob, fileName, function(responseText) {
  // xhr(server_address+'/api/support/streamRecordings/', blob, fileName, function(responseText) {
    element.srcObject = mediaStream;
    element.play();
    element.muted = false;
    call_back(agent)
  });
}


function xhr(url, data, name, callback) {
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
      callback(request.responseText);
    }
  };

  request.open('POST', url);
  var formData = new FormData();
  formData.append('file', data);
  formData.append('fileName', name);
  request.send(formData);
}

$(document).ready(function() {

  meeting = new Meeting(host);
  if (audio_video == 'video') {

    document.getElementById('mute').addEventListener('click', function() {
      videMicOn = ! videMicOn
      meeting.toggleMic()
    })

    document.getElementById('pause').addEventListener('click', function() {
      var vid = document.getElementById('remoteVideo')
      if (vid.paused) {
        vid.play();
      } else {
        vid.pause();
      }
    })
  }


  window.onbeforeunload = function() {
    console.log('winodw refreshed');
    // alert('you are')
    closeChat();
  }

  var callingElement, ring;
  var isCallPicked = false;



  meeting.onLocalVideo(function(stream) {

    console.log('on local video **********************');

    mediaStream = stream;
    localStream = stream;

    if (audio_video == 'video') {
      console.log('in adding local video');
      parent.postMessage('timeToStart', "*")
      callingElement = document.getElementById('calling')
      ring = document.createElement('img');
      ring.src = '/img/P3tY.gif';
      callingElement.append(ring);
      localVideoElement = document.querySelector('video');
      let myVideo = document.querySelector('#localVideo')
      if (myVideo.mozSrcObject !== undefined) { // FF18a
        myVideo.mozSrcObject = localStream;
      } else if (myVideo.srcObject !== undefined) {
        myVideo.srcObject = localStream;
      } else { // FF16a, 17a
        myVideo.src = localStream;
      }
      // myVideo.srcObject = localStream;
      console.log(document.querySelector('#localVideo'));
    }
    if (audio_video == 'audio') {
      console.log('in adding local audio');
      parent.postMessage('timeToStart', "*")
    }
    setTimeout(function() {
      if (!isCallPicked) {
        // alert('unable to answer');
        localStream.stop();
        parent.postMessage("dintPickTheCall", '*');
        isCallPicked = false;
      }
    }, 40000);
  });





  meeting.onRemoteVideo(function(stream, participantID) {

    var localVideo = document.getElementById("localVideo");
    if (localVideo) {
      localVideo.muted = true;
    }

    isCallPicked = true //checking for the anwser from agent
    myParticipantId = participantID
    console.log('Setting myParticipantId to : ', myParticipantId);
    if (audio_video == 'video') {
      callingElement.removeChild(ring) //remove the bell image
    }
    if (audio_video == 'audio') {
      document.getElementById('RingHtml').innerHTML = 'Connected'; //remove ringing from audio element
    }
    addRemoteVideo(stream, participantID);
  });

  meeting.onParticipantHangup(function(participantID) {
    removeRemoteVideo(participantID);
  });

  meeting.onChatReady(function() {
    console.log("Chat is ready");
  });

  meeting.onChatNotReady(function() {
    console.log("Chat is not ready");
    closeChat();
  });

  // console.log(room, 'TRRRRRRRRRRr');
  meeting.joinRoom(room, audio_video);

});

function addRemoteVideo(stream, participantID) {
  setTimeout(function() {
    remoteStream = stream;
    canvas_agent = document.getElementById('canvas_agent');
    ctx_agent = canvas_agent.getContext('2d');
    canvas_agent.width = w;
    canvas_agent.height = h;
    console.log(localStream);
    console.log(remoteStream);
    if (audio_video == 'audio') {
      // if(remoteStream.getTracks()[0].kind=='video'){
      // 		console.log('here');
      // }
      document.querySelector('#localAudio').srcObject = remoteStream;
      console.log(audioElement);
    } else {
      var $videoBox = $("<div class='videoWrap' id='" + participantID + "'></div>");
      var $video = $("<video class='$videoBox' style='object-fit:fill;width:99vw;height:91vh;' id='remoteVideo' controls='true' playsinline autoplay></video>");
      $videoBox.append($video);
      console.log($video);
      $("#videosWrapper").append($videoBox);
      remoteVideoElement = document.getElementById('remoteVideo');
      // remoteVideoElement.muted = true;
      if (remoteVideoElement.mozSrcObject !== undefined) { // FF18a
        remoteVideoElement.mozSrcObject = remoteStream;
      } else if (remoteVideoElement.srcObject !== undefined) {
        remoteVideoElement.srcObject = remoteStream;
      } else { // FF16a, 17a
        remoteVideoElement.src = remoteStream;
      }
      remoteVideoElement.removeAttribute("controls");
      let localVideo = document.getElementById('localVideo')
      if (localVideo) {
        localVideo.removeAttribute("controls");
      }
      // remoteVideoElement.srcObject = remoteStream;
    }

    if (isAgent) {
      recorder1 = RecordRTC(localStream, {
        type: audio_video
      });
      recorder1.startRecording();

      recorder = RecordRTC(remoteStream, {
        type: audio_video,
      });
      recorder.startRecording();
    }

    adjustVideoSize();

  }, 1500);
}

function removeRemoteVideo(participantID) {
  $("#" + participantID).remove();
  adjustVideoSize();
}

function adjustVideoSize() {
  var numOfVideos = $(".videoWrap").length;
  if (numOfVideos > 2) {
    var $container = $("#videosWrapper");
    var newWidth;
    for (var i = 1; i <= numOfVideos; i++) {
      newWidth = $container.width() / i;
      var scale = newWidth / $(".videoWrap").width();
      var newHeight = $(".videoWrap").height() * scale;
      var columns = Math.ceil($container.width() / newWidth);
      var rows = numOfVideos / columns;
      if ((newHeight * rows) <= $container.height()) {
        break;
      }
    }
    var percent = (newWidth / $container.width()) * 100;
    $(".videoWrap").css("width", percent - 5 + "%");
    $(".videoWrap").css("height", "auto");
    var numOfColumns;
    for (var i = 2; i <= numOfVideos; i++) {
      if (numOfVideos % i === 0) {
        numOfColumns = i;
        break;
      }
    }
    $('#videosWrapper').find("br").remove();
    $('.videoWrap:nth-child(' + numOfColumns + 'n)').after("<br>");
  } else if (numOfVideos == 2) {
    $(".videoWrap").width('100%');
    $("#localVideoWrap").css("width", 20 + "%");
    $('#videosWrapper').find("br").remove();
  } else {
    $("#localVideoWrap").width('auto');
    $('#videosWrapper').find("br").remove();
  }
}

function calledToStop() {

  screen = !screen; // change screen if click on screen sharing

  if (screen) {

    getChromeExtensionStatus(function(status) {
      // if (status === 'installed-enabled') alert('installed');
      if (status === 'installed-disabled') alert('installed but disabled');
      if (status == 'not-installed') {
        if (confirm("To use this feature please install Chrome extension and Reconnect")) {
          setTimeout(function() {
            parent.postMessage("loadMyOrders", "*");
          }, 1000);
          // window.location = "https://www.google.com";
          // change this later
        } else {
          txt = "You pressed Cancel!";
        }
        return;
      }
      // etc.
    });


    document.getElementById('screenSharing').style.color = "green"
    getScreenId(function(error, sourceId, screen_constraints) {
      // error    == null || 'permission-denied' || 'not-installed' || 'installed-disabled' || 'not-chrome'
      // sourceId == null || 'string' || 'firefox'

      if (navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob)) {
        navigator.getDisplayMedia(screen_constraints).then(stream => {
          document.querySelector('video').srcObject = stream;
        }, error => {
          alert('Please make sure to use Edge 17 or higher.');
        });
        return;
      }
      setTimeout(function() {
        console.log(screen_constraints);
        navigator.mediaDevices.getUserMedia(screen_constraints).then(function(stream) {
          console.log(stream);
          if (isAgent) {
            recorder2 = RecordRTC(stream, {
              type: audio_video
            });
            recorder2.startRecording(); // start recording for screen sharing
          }


          document.querySelector('#localVideo').srcObject = stream;
          console.log(myParticipantId);
          connection = meeting.currentConnection[myParticipantId];
          console.log(connection);
          if (connection) {
            mySender = connection.getSenders()[1];
            console.log(mySender);
            mySender.replaceTrack(stream.getTracks()[0]);
          }

        }).catch(function(error) {
          console.error(error);
        });
      }, 1000);

    })
  } else {
    console.log('stop stream recordinggggggggggggggggggggggggg ****************8');
    document.getElementById('screenSharing').style.color = ""
    connection = meeting.currentConnection[myParticipantId];
    mySender = connection.getSenders()[1];
    document.querySelector('#localVideo').srcObject = localStream;
    console.log(localStream.getTracks());
    mySender.replaceTrack(localStream.getTracks()[1]);
    setTimeout(function() {
      if (isAgent) {
        stopStreamRecording(recorder2, 'screen');
      }
    }, 1800);

  }
}

function max_window() {
  var vid = document.querySelector('#remoteVideo');
  if (vid) {
    if (vid.requestFullScreen) {
      vid.requestFullScreen();
    } else if (vid.webkitRequestFullScreen) {
      vid.webkitRequestFullScreen();
    } else if (vid.mozRequestFullScreen) {
      vid.mozRequestFullScreen();
    }
  }
}
